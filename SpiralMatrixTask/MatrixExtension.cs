﻿using System;

#pragma warning disable CA1814
#pragma warning disable S2368

namespace SpiralMatrixTask
{
    public static class MatrixExtension
    {
        public static int[,] GetMatrix(int size)
        {
            if (size <= 0)
            {
                throw new ArgumentException("Matrix size is less or equal zero.");
            }

            int[,] matrix = new int[size, size];
            int value = 1;
            int endRow = size;
            int endColumn = size;
            int startRow = 0;
            int startColumn = 0;

            while (startRow < endRow)
            {
                for (int i = startColumn; i < endColumn; ++i)
                {
                    matrix[startColumn, i] = value++;
                }

                startRow++;

                for (int i = startRow; i < endRow; ++i)
                {
                    matrix[i, endRow - 1] = value++;
                }

                endColumn--;

                for (int i = endColumn - 1; i >= startColumn; --i)
                {
                    matrix[endRow - 1, i] = value++;
                }

                endRow--;

                for (int i = endRow - 1; i >= startRow; --i)
                {
                    matrix[i, startColumn] = value++;
                }

                startColumn++;
            }

            return matrix;
        }
    }
}
